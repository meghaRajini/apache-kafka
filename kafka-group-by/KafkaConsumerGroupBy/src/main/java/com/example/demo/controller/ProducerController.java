package com.example.demo.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;


import com.example.demo.dto.TransactionDto;
import com.example.demo.service.ProducerService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class ProducerController {
	
	private final ProducerService producerService;
	
	@PostMapping("/produce-events")
	public void produce(@RequestParam String key,@RequestBody TransactionDto transactionDto)
	{
		producerService.produce(key, transactionDto);
	}

}
