package com.example.demo.serdes;

import org.apache.kafka.common.serialization.Serdes;

import com.example.demo.deserializer.CompositeKeyDeserializer;
import com.example.demo.dto.CompositeKey;
import com.example.demo.serializer.CompositeKeySerializer;

public class CompositeKeySerde extends Serdes.WrapperSerde<CompositeKey>{
	public CompositeKeySerde() {
		super(new CompositeKeySerializer(), new CompositeKeyDeserializer());
	}


}
