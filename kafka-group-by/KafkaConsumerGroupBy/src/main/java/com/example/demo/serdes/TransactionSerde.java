package com.example.demo.serdes;

import org.apache.kafka.common.serialization.Serdes;

import com.example.demo.deserializer.KafkaDeserializer;
import com.example.demo.dto.TransactionDto;
import com.example.demo.serializer.KafkaSerilaizer;

public class TransactionSerde extends Serdes.WrapperSerde<TransactionDto>{
	public TransactionSerde() {
		super(new KafkaSerilaizer(), new KafkaDeserializer());
	}



	



}
