package com.example.demo.configuration;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TopicConfiguration {
	
	
	@Bean
	NewTopic createTopic1()
	{
		return new NewTopic("kafka-transaction",3,(short)1);
	}

}
