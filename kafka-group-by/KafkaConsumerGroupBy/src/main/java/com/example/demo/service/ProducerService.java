package com.example.demo.service;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import com.example.demo.dto.TransactionDto;
import com.example.demo.serializer.KafkaSerilaizer;

@Service
public class ProducerService {
    public void produce(String key,TransactionDto transactionDto)
    {
    	Properties properties=new Properties();
    	properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
    	properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
    	properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG,KafkaSerilaizer.class);
    	properties.put(ProducerConfig.ACKS_CONFIG, "all");
    	
    	
    	try(KafkaProducer<String, TransactionDto> producer=new KafkaProducer<>(properties)) {
    		final ProducerRecord<String, TransactionDto> producerRecord=new ProducerRecord<String, TransactionDto>("kafka-transaction",key,transactionDto);
    		producer.send(producerRecord);
    	}
    }
	
}
