package com.example.demo.serializer;

import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Serializer;


import com.example.demo.dto.TransactionDto;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KafkaSerilaizer implements Serializer<TransactionDto>{
	
	
		@Override
		public byte[] serialize(String topic, TransactionDto data) {
			
			if(Objects.isNull(data)) {
				log.error("null value received at serializer");
				return null;
			}
			ObjectMapper mapper = new ObjectMapper();
			mapper.registerModule(new JavaTimeModule());
			try {
				return mapper.writeValueAsBytes(data);
			} catch (JsonProcessingException e) {
				throw new SerializationException("error occurred while serialization");
			}
		}



}
