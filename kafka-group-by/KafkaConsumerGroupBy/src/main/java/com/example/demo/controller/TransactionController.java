package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.TransactionService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class TransactionController {
	
	private final TransactionService transactionService;
	
	@GetMapping("/transactions")
	public void run()
	{
		transactionService.run();
	}

}
