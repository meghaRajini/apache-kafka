package com.example.demo.deserializer;

import java.io.IOException;
import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.example.demo.dto.TransactionDto;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class KafkaDeserializer implements Deserializer<TransactionDto>{
	@Override
	public TransactionDto deserialize(String topic, byte[] data) {
		
		if(Objects.isNull(data)) {
			log.error("null value received at deserializer");
			return null;
		}
		ObjectMapper mapper = new ObjectMapper();
		mapper.registerModule(new JavaTimeModule());
		try {
			return mapper.readValue(data, TransactionDto.class);
		} catch (IOException e) {
			throw new SerializationException("error occurred while deserialization");
		}
	}


}
