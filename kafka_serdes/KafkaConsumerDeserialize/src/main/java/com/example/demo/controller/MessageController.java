package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.KafkaMessageService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class MessageController {
	
	private final KafkaMessageService service;
	@GetMapping("/consume")
	public void consumeMessage()
	{
		service.consumeEvents();
	}

}
