package com.example.demo.config;

import java.io.IOException;
import java.util.Objects;

import org.apache.commons.lang3.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.example.demo.dto.Customer;
import com.fasterxml.jackson.databind.ObjectMapper;

public class KafkaDeserializer implements Deserializer<Customer>{

	@Override
	public Customer deserialize(String topic, byte[] data) {
		
		ObjectMapper mapper = new ObjectMapper();
		if(Objects.isNull(data)) {
			return null;
		}
		else {
			try {
				return mapper.readValue(data, Customer.class);
			} catch (IOException e) {
				throw new SerializationException("error occurred while deserialization");
			}
		}
	}

}
