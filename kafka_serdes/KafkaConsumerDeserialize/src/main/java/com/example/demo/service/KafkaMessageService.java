package com.example.demo.service;

import java.time.Duration;
import java.util.Collections;
import java.util.Properties;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerConfig;
import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.apache.kafka.common.serialization.StringDeserializer;
import org.springframework.stereotype.Service;

import com.example.demo.dto.Customer;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class KafkaMessageService {

	public void consumeEvents() {

		Properties properties = new Properties();
		properties.put(ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		properties.put(ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG, StringDeserializer.class);
		properties.put(ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG, true);
		properties.put(ConsumerConfig.AUTO_OFFSET_RESET_CONFIG, "earliest");
		properties.put(ConsumerConfig.GROUP_ID_CONFIG, "new-group");

		Consumer<String, Customer> consumer = new KafkaConsumer<>(properties);
		consumer.subscribe(Collections.singleton("message"));
		while (true) {
			consumer.poll(Duration.ofSeconds(1)).forEach(message -> {
				log.info("message received: {}", message.value());
			});

		}

	}

}
