package com.example.demo.controller; 
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.dto.Customer;
import com.example.demo.service.KafkaMessageProducer;



import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class EventController {
	
	private final KafkaMessageProducer producer;
	
	@PostMapping("/send-event")
	public void sendEvents(@RequestBody Customer customer)
	{
		producer.sendProducer(customer);
	}
	
	



}
