package com.example.demo.service;

import java.util.Properties;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.example.demo.config.KafkaSerializer;
import com.example.demo.dto.Customer;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class KafkaMessageProducer {

	private final KafkaTemplate<String, Object> template;

	public void sendProducer(Customer customer) {
		Properties props = new Properties();
		props.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		props.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		props.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, KafkaSerializer.class);
		props.put(ProducerConfig.ACKS_CONFIG, "all");

		KafkaProducer<String, Customer> producer = new KafkaProducer<>(props);

		ProducerRecord<String, Customer> producerRecord = new ProducerRecord<String, Customer>("message", customer);

		producer.send(producerRecord, ((result, exception) -> {
			if (exception == null) {
				System.out.println("Sent message=[" + customer.toString() + "] with offset=[" + result.offset() + "]");

			} else {
				System.out.println(
						"Unable to send message=[" + customer.toString() + "]due to : " + exception.getMessage());
			}

		}));

	}

}
