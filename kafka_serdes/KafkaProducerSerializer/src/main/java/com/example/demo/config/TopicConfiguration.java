package com.example.demo.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TopicConfiguration {
	@Bean
  NewTopic createTopic() {
		return new NewTopic("message",3,(short)1);
	}

}
