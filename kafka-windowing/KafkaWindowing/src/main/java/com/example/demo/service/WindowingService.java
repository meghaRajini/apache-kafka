package com.example.demo.service;

import java.time.Duration;
import java.util.Properties;

import org.apache.kafka.common.serialization.Serdes;
import org.apache.kafka.streams.KafkaStreams;
import org.apache.kafka.streams.StreamsBuilder;
import org.apache.kafka.streams.StreamsConfig;
import org.apache.kafka.streams.kstream.Consumed;
import org.apache.kafka.streams.kstream.KStream;
import org.apache.kafka.streams.kstream.SessionWindows;
import org.apache.kafka.streams.kstream.Suppressed;
import org.apache.kafka.streams.kstream.TimeWindows;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class WindowingService {

	private static Properties windowOperations() {
		Properties properties = new Properties();
		properties.put(StreamsConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(StreamsConfig.APPLICATION_ID_CONFIG, "window-operation");
		properties.put(StreamsConfig.DEFAULT_KEY_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		properties.put(StreamsConfig.DEFAULT_VALUE_SERDE_CLASS_CONFIG, Serdes.String().getClass());
		return properties;
	}

	public void executeHopingWindow() {
		StreamsBuilder builder = new StreamsBuilder();
		KStream<String, String> stream = builder
				.stream("hoping-window", Consumed.with(Serdes.String(), Serdes.String()))
				.peek((key, value) -> log.info("Incoming stream key:{} and value:{}", key, value));

		Duration windowSize = Duration.ofSeconds(10);
		Duration advanceSize = Duration.ofSeconds(5);

		stream.groupByKey().windowedBy(TimeWindows.ofSizeWithNoGrace(windowSize).advanceBy(advanceSize)).count()
				.suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded())).toStream()
				.peek((key, value) -> log.info("Windowed key:{} and count:{}", key.key(), value));

		KafkaStreams kafkaStreams = new KafkaStreams(builder.build(), windowOperations());
		kafkaStreams.start();
	}

	public void executeTumblingWindow() {
		StreamsBuilder builder1 = new StreamsBuilder();
		KStream<String, String> stream1 = builder1
				.stream("tumbling-window", Consumed.with(Serdes.String(), Serdes.String()))
				.peek((key, value) -> log.info("Incoming key:{} and value:{}", key, value));

		stream1.groupByKey().windowedBy(TimeWindows.ofSizeWithNoGrace(Duration.ofSeconds(10))).count()
				.suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded())).toStream()
				.peek((key, value) -> log.info("Windowed key:{} and count:{}", key.key(), value));

		KafkaStreams kafkaStreams = new KafkaStreams(builder1.build(), windowOperations());
		kafkaStreams.start();

	}

	public void executeSessionWindow() {
		StreamsBuilder builder2 = new StreamsBuilder();
		KStream<String, String> streams2 = builder2
				.stream("session-window", Consumed.with(Serdes.String(), Serdes.String()))
				.peek((key, value) -> log.info("Incoming key:{} and value:{}", key, value));
		streams2.groupByKey().windowedBy(SessionWindows.ofInactivityGapWithNoGrace(Duration.ofSeconds(20))).count()
				.suppress(Suppressed.untilWindowCloses(Suppressed.BufferConfig.unbounded())).toStream()
				.peek((key, value) -> log.info("Windowed key:{} and count:{}", key, value));

		KafkaStreams kafkaStreams = new KafkaStreams(builder2.build(), windowOperations());
		kafkaStreams.start();

	}

}
