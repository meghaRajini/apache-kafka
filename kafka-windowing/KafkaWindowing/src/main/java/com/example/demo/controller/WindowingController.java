package com.example.demo.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.WindowingService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class WindowingController {
	
	private final WindowingService windowService;
	@GetMapping("/hoping-window")
	public void hopingWindow()
	{
		windowService.executeHopingWindow();
	}
	
	@GetMapping("/tumbling-window")
	public void tumblingWindow()
	{
		windowService.executeTumblingWindow();
	}
	
	@GetMapping("/session-window")
	public void sessionWindow()
	{
		windowService.executeSessionWindow();
	}

}
