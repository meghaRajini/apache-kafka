package com.example.demo.config;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class TopicConfiguration {
	
	@Bean
	NewTopic createTopic()
	{
		return new NewTopic("hoping-window",3,(short)1);
	}
	
	@Bean
	NewTopic createTopic1()
	{
		return new NewTopic("tumbling-window",3,(short)1);
	}
	
	@Bean
	NewTopic createTopic2()
	{
		return new NewTopic("session-window",3,(short)1);
	}

}
