package com.example.demo.service;

import java.util.Objects;
import java.util.Properties;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class MessageProducer {
	
	public Future<RecordMetadata> produce(final String message)
	{
		Properties properties=new Properties();
		properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, "localhost:9092");
		properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
		properties.put(ProducerConfig.ACKS_CONFIG, "all");
		
		try (KafkaProducer<String, String> producer=new KafkaProducer<>(properties)){
			final String[] parts=message.split("-");
			final String key,value;
			if(parts.length>0)
			{
				key=parts[0];
				value=parts[1];
			}
			else
			{
				key=null;
				value=parts[0];
			}
			final ProducerRecord<String, String> producerRecord=new ProducerRecord<String, String>("message",key, value);
			return producer.send(producerRecord,(recordMetaData, exception)->{
				if(Objects.isNull(exception)) {
					log.info("Record with key:{} and value:{} is written to partition: {} and offset: {}",key, value, recordMetaData.partition(), recordMetaData.offset());
				}
				else {
					log.error("Error ocurred while writing data to topic");
				}

			});
			
		}
		
	}

}
