package com.example.demo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.MessageProducer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ProducerController {
	
	private final MessageProducer messageProducer;
	
	@PostMapping("/produce")
	public void sendCustomer()
	{
		try {
			String filePath = "input.txt";
			List<String> lines = Files.readAllLines(Paths.get(filePath));
			List<Future<RecordMetadata>> metadata = lines.stream()
					.map(messageProducer::produce)
					.toList();

		}
		catch(IOException e)
		{
			log.error("Invalid file path");
			e.printStackTrace();

		}
	}

}
