package com.example.demo.service;


import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.annotation.TopicPartition;
import org.springframework.stereotype.Service;

import com.example.demo.dto.MessageDto;

import lombok.extern.slf4j.Slf4j;


@Service
@Slf4j
public class MessageReader {
	@KafkaListener(topics = "message",  
			containerFactory = "concurrentKafkaListenerContainerFactory")

	public void consumeFromPartitionZero(MessageDto messageDto)
	{
		log.info("Message received: {} from partition 0", messageDto.getEmail());
	}


}
