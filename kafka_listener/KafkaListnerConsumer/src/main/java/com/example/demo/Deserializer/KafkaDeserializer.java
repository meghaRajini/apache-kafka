package com.example.demo.Deserializer;

import java.util.Objects;

import org.apache.kafka.common.errors.SerializationException;
import org.apache.kafka.common.serialization.Deserializer;

import com.example.demo.dto.MessageDto;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;
@Slf4j
public class KafkaDeserializer implements Deserializer<MessageDto>{
	@Override
	public MessageDto deserialize(String topic, byte[] data) {
		
		ObjectMapper mapper = new ObjectMapper();
		if(Objects.isNull(data)) {
			log.error("null value received while deserialization");
			return null;
		} else {
			try {
				return mapper.readValue(data, MessageDto.class);
			} catch (Exception e) {
				log.error("error occurred while deserialization");
				throw new SerializationException("error occurred while deserialization");
			}
		}
	}

}
