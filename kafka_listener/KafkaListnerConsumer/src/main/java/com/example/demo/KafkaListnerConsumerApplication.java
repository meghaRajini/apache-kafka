package com.example.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KafkaListnerConsumerApplication {

	public static void main(String[] args) {
		SpringApplication.run(KafkaListnerConsumerApplication.class, args);
	}

}
