package com.example.demo.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.service.MessageProducer;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class ProducerController {
	
	private final MessageProducer messageProducer;
	
	@PostMapping("/events")
	public void sendCustomer()
	{
		messageProducer.produce("Producer sent message Successfully");
	}

}
