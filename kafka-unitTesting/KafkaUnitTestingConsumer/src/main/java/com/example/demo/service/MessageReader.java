package com.example.demo.service;

import java.nio.file.Paths;
import java.time.Duration;
import java.util.Collections;

import org.apache.kafka.clients.consumer.Consumer;
import org.apache.kafka.clients.consumer.ConsumerRecords;

import com.example.demo.utils.ConsumerRecordFileWritingService;
import com.example.demo.utils.ConsumerRecordHandler;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MessageReader {
	
	final ConsumerRecordHandler<String, String> consumerRecordHandler;
	final Consumer<String, String> consumer;

	public MessageReader(String filePath, Consumer<String, String> consumer) {
		this.consumer = consumer;
		this.consumerRecordHandler = new ConsumerRecordFileWritingService(Paths.get(filePath));

	}

	private volatile boolean consume = true;

	public void runConsumer() {

		consumer.subscribe(Collections.singletonList("message"));
		while (consume) {
			final ConsumerRecords<String, String> records = consumer.poll(Duration.ofSeconds(1));
			consumerRecordHandler.process(records);
		}

	}
	
	public void shutdown() {
		consume = false;
	}

	

}
