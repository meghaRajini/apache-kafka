package com.example.demo.controller;

import org.apache.kafka.clients.consumer.KafkaConsumer;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.configuration.ConsumerConfiguration;
import com.example.demo.service.MessageReader;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
public class MessageController {
	
	final ConsumerConfiguration configuration = new ConsumerConfiguration();

	private final MessageReader messageReader = new MessageReader("output.txt", new KafkaConsumer<>(configuration.consumerProperties()));
	
	@GetMapping("/consume")
	public void consumeMessages() {
		Runtime.getRuntime().addShutdownHook(new Thread(messageReader::shutdown));
		messageReader.runConsumer();
	}

	
	
	

}
