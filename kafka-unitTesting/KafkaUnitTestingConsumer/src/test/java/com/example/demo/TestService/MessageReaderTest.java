package com.example.demo.TestService;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.apache.kafka.clients.consumer.MockConsumer;
import org.apache.kafka.clients.consumer.OffsetResetStrategy;
import org.apache.kafka.common.TopicPartition;
import org.junit.jupiter.api.Test;

import com.example.demo.service.MessageReader;

public class MessageReaderTest {
	final MockConsumer<String, String> mockConsumer = new MockConsumer<>(OffsetResetStrategy.EARLIEST);
	final MessageReader messageReader = new MessageReader("output-test.txt", mockConsumer);
	final String topic = "message";
	
	@Test
	public void testConsumer() throws IOException {
		
		mockConsumer.schedulePollTask(() -> {
			addTopicAssignment("message", mockConsumer, new TopicPartition(topic, 0));
			mockConsumer.addRecord(new ConsumerRecord<String, String>(topic, 0, 0, null, "hi"));
			mockConsumer.addRecord(new ConsumerRecord<String, String>(topic, 0, 1, null, "hello"));
			mockConsumer.addRecord(new ConsumerRecord<String, String>(topic, 0, 2, null, "how are you"));
		});
		
		mockConsumer.schedulePollTask(messageReader::shutdown);
		messageReader.runConsumer();
		
		List<String> actualResult = Arrays.asList("hi", "hello", "how are you");
		List<String> expectedResult = Files.readAllLines(Paths.get("output-test.txt"));
		assertEquals(actualResult.size(), expectedResult.size());
		assertEquals(actualResult.get(0), expectedResult.get(0));
	}
	
	private void addTopicAssignment(String topic, final MockConsumer<String, String> conumer, final TopicPartition partition) {
		
		final Map<TopicPartition, Long> beginningOffset = new HashMap<>();
		beginningOffset.put(partition, 0l);
		mockConsumer.rebalance(Collections.singleton(new TopicPartition(topic, 0)));
		mockConsumer.updateBeginningOffsets(beginningOffset);
		
		
	}


}
