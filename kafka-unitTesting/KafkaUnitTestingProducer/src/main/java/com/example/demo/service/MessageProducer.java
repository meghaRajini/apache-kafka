package com.example.demo.service;

import java.util.List;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.clients.producer.RecordMetadata;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class MessageProducer{
	
	
	private final Producer<String, String> kafkaProducer;

	public MessageProducer(Producer<String, String> producer) {
		this.kafkaProducer = producer;
	}

	public Future<RecordMetadata> produce(final String message) {

		final String[] parts = message.split("-");
		final String key, value;
		if (parts.length > 1) {
			key = parts[0];
			value = parts[1];
		} else {
			key = null;
			value = parts[0];
		}

		final ProducerRecord<String, String> producerRecord = new ProducerRecord<String, String>("message",
				key, value);
		return kafkaProducer.send(producerRecord);

	}

	public void printMetadata(final List<Future<RecordMetadata>> metadata, final String filePath) {

		log.info("Offset and partition of each data: ");
		metadata.forEach(data -> {
			try {
				final RecordMetadata recordMetadata = data.get();
				log.info("Record: {} written to partition: {} and offset: {}", recordMetadata.partition(),
						recordMetadata.offset());
			} catch (Exception e) {
				e.printStackTrace();
			}
		});
	}





	
	

	
}
