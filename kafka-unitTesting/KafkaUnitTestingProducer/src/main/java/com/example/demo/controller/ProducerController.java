package com.example.demo.controller;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.concurrent.Future;

import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.RecordMetadata;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.configuration.ProducerConfiguration;
import com.example.demo.service.MessageProducer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RestController
@RequiredArgsConstructor
@Slf4j
public class ProducerController {

	 private final ProducerConfiguration configuration;
	
	private MessageProducer messageProducer;
	
	@PostMapping("/events")
	public void sendCustomer() {
		
		try {
			messageProducer = new MessageProducer(new KafkaProducer<>(configuration.producerCongifurations()));
			String filePath = "input.txt";
			List<String> lines = Files.readAllLines(Paths.get(filePath));
			List<Future<RecordMetadata>> metadata = lines.stream()
					.map(messageProducer::produce)
					.toList();
			messageProducer.printMetadata(metadata, filePath);
		} catch (IOException e) {
			log.error("Invalid file path");
			e.printStackTrace();
		}
	}



}
