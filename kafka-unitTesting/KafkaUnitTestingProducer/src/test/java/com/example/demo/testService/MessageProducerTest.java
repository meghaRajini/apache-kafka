package com.example.demo.testService;



import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Arrays;
import org.apache.kafka.clients.producer.MockProducer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.apache.kafka.common.serialization.StringSerializer;

import org.junit.jupiter.api.Test;

import com.example.demo.service.MessageProducer;



public class MessageProducerTest {
	MockProducer<String, String> mockProducer = new MockProducer<>(true, new StringSerializer(),
			new StringSerializer());

	MessageProducer messageProducer = new MessageProducer(mockProducer);

	@Test
	public void testProducer() {

		List<String> records = Arrays.asList("foo-bar", "megha-r", "divya-sk");

		records.stream().map(messageProducer::produce).toList();

		ProducerRecord<String, String> firstRecord = mockProducer.history().get(0);
		assertEquals(mockProducer.history().size(), 3);
		assertEquals(firstRecord.key(), "foo");
		assertEquals(firstRecord.value(), "bar");

	}

		
		


	}


